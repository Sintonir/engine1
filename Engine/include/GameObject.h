//
// Created by Саша on 23.08.2018.
//
#include "Animation.h"
#include<unordered_set>
#ifndef UNTITLED_GAMEOBJECT_H
#define UNTITLED_GAMEOBJECT_H

class GameObject {
public:
	virtual const AbstractImage& getFrameByNumber(int i) = 0;

	bool isVisible(int frame) const;
	void setVisibility(int start, int finish);

	unsigned int getWidth();
	unsigned int getHeight();
	void setSize(unsigned int w, unsigned int h);

	int getY() const;
	void setY(int y);
	int getX() const;
	void setX(int x);
	void setRenderRect(int x, int y,unsigned int width, unsigned int height );

	int getID() const;
	int getZOrder() const;
	void setZOrder(int z);

protected:
	GameObject(int lapse, int x, int y, int zorder, int width, int height, int start, int finish);
	GameObject(GameObject& that) = default;
	virtual bool isFrameEmpty() const = 0;
	static int getNextID();

	int zOrder_ = 0;
	Rect renderRect_;
	Timespan visibleInterval_;
	int ID_;
	int lapse_ = 1000;

	static int OBJ_ID_;
};

class ImageObject :public GameObject
{
public:
	ImageObject(pAImage &&image, int x, int y, int zorder, int w, int h, int start, int finish);
	const	AbstractImage &getFrameByNumber(int i) override;

private:
	pAImage image;
	bool isFrameEmpty()const override;
	ImageObject(ImageObject& that) = default;

};

class AnimObject :public GameObject{
public:
	AnimObject(int lapse, int x, int y, int z, int w, int h, int start, int finish);
	
	const	AbstractImage & getFrameByNumber(int i) override;
	void addImage(pAImage &&image);

private:
	Animation anim;
	bool isFrameEmpty() const override;
	AnimObject(AnimObject& that) = default;
};

typedef std::unique_ptr<GameObject> pGameObj;

#endif //UNTITLED_GAMEOBJECT_H
