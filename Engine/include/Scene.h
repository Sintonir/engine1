//
// Created by Саша on 23.08.2018.
//

#ifndef UNTITLED_SCENE_H
#define UNTITLED_SCENE_H
#include "GameObject.h"
#include <unordered_map>
#include <set>
class Scene {
public:
	Scene();
	bool draw(AbstractRender &render, int FrameNum);
	int addObj(pGameObj &&obj);
	std::vector<int>::iterator GetIteratorByZ(int z);
	bool removeObj(int ID);
	int getControlID();
	int getLength();

	void moveControlledUp();
	void moveControlledDown();
	void moveControlledLeft();
	void moveControlledRight();
	void changeControl(int num);

	void reZOrderControl(int newZ);

private:
	std::unordered_map<int, pGameObj> gameObjs_;
	std::vector <int> zSortedKeys_;
	int controlID_ = 0;
	int currFrame_ = 0;
	unsigned short winWidth_ = 800;
	unsigned short winHeight_ = 600;
	const static unsigned short defaultPixelMove = 10;
};


#endif //UNTITLED_SCENE_H
