//
// Created by Саша on 23.08.2018.
//

#ifndef UNTITLED_ENGINE_H
#define UNTITLED_ENGINE_H
#define FRAMESEC  10

#include "Scene.h"
#include "Events.h"
#include "../render/SFRender.h"
#include <tuple>

class Engine :public Events::AbstractListener {
public:
	virtual ~Engine();
	Engine(unsigned int lapse = 0, unsigned int winWidth = 0, unsigned int winHeight = 0);
	int addObject(const std::string &path,
		int x = 0, int y = 0, int zorder = 0,
		unsigned int width = 0, unsigned int height = 0,
		int start = 0, int finish = 0,
		int lapse = 0
	);
	int addObject(const std::vector< std::string> &paths,
		int x = 0, int y = 0, int zorder = 0,
		unsigned int width = 0, unsigned int height = 0,
		int start = 0, int finish = 0,
		int lapse = 0
	);


	void startApp();
private:
	pAImage loadImage(const std::string &path);

	void onKeyPressed(Events::KeyEvent key) override;
	void onKeyReleased(Events::KeyEvent key) override;
	void onDraw(int frameNum) override;
	void eventHandle(Events::Event event) override;

	std::shared_ptr<Scene> scene;
	std::shared_ptr<AbstractRender> render;
	int currFrame = 0;

	bool up_pressed_ = 0;
	bool down_pressed_ = 0;
	bool left_pressed_ = 0;
	bool right_pressed_ = 0;
	int tab_pressed_ = 0;

	const unsigned int lapse_;
	const unsigned short winWidth_;
	const unsigned short winHeight_;

	const static unsigned short defaultLapse = 5000;
	const static unsigned short defaultVMove = 100;
	const static unsigned short defaultWinWidth = 1000;
	const static unsigned short defaultWinHeight = 300;
};


#endif //UNTITLED_ENGINE_H
