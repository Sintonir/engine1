//
// Created by Саша on 02.09.2018.
//

#ifndef ENGINE0_XMLWORK_H
#define ENGINE0_XMLWORK_H

#include "pugixml.hpp"
#include "Engine.h"
#include <fstream>

class XMLWork {
public:
	std::unique_ptr<Engine> loadFromXML(std::string path);
	bool exampleToXML();
};


#endif //ENGINE0_XMLWORK_H
