//
// Created by Саша on 01.09.2018.
//
#ifndef ENGINE0_EVENTS_H
#define ENGINE0_EVENTS_H
namespace Events {
	enum Key {
		Unknown = -1, A = 0, B, C,
		D, E, F, G,
		H, I, J, K,
		L, M, N, O,
		P, Q, R, S,
		T, U, V, W,
		X, Y, Z, Num0,
		Num1, Num2, Num3, Num4,
		Num5, Num6, Num7, Num8,
		Num9, Escape, LControl, LShift,
		LAlt, LSystem, RControl, RShift,
		RAlt, RSystem, Menu, LBracket,
		RBracket, Semicolon, Comma, Period,
		Quote, Slash, Backslash, Tilde,
		Equal, Hyphen, Space, Enter,
		Backspace, Tab, PageUp, PageDown,
		End, Home, Insert, Delete,
		Add, Subtract, Multiply, Divide,
		Left, Right, Up, Down,
		Numpad0, Numpad1, Numpad2, Numpad3,
		Numpad4, Numpad5, Numpad6, Numpad7,
		Numpad8, Numpad9, F1, F2,
		F3, F4, F5, F6,
		F7, F8, F9, F10,
		F11, F12, F13, F14,
		F15, Pause, KeyCount
	};

	struct KeyEvent {
		Key keyCode;
		bool alt;
		bool control;
		bool shift;

		KeyEvent(Key keyCode, bool alt, bool control, bool shift) : keyCode(keyCode), alt(alt), control(control),
			shift(shift) {}
	};
	enum Button {
		LeftB, RightB, Middle, XButton1,
		XButton2, ButtonCount
	};

	struct MouseButton {

		MouseButton(Button button, int x, int y) : button(button), x(x), y(y) {}

		Button button;
		int x;
		int y;
	};

	struct MouseMove {
		int x;

		int y;

		MouseMove(int x, int y) : x(x), y(y) {}
	};

	struct MouseWheel {
		bool isHorisontal;
		float offset;
		int x;
		int y;

		MouseWheel(bool isHorisontal, float offset, int x, int y) : isHorisontal(isHorisontal), offset(offset), x(x),
			y(y) {}

	};

	enum EventType {
		keyPressed,
		keyReleased,
		mouseButtonPressed,
		mouseButtonReleased,
		mouseMove,
		mouseWheel
	};

	struct Event {
		EventType type;

		Event(EventType type) : type(type) {};

		Event(EventType type, const KeyEvent &key) : type(type), key(key) {}

		Event(EventType type, const MouseButton &mouse) : type(type), mouse(mouse) {}

		Event(EventType type, const MouseMove &move) : type(type), move(move) {}

		Event(EventType type, const MouseWheel &wheel) : type(type), wheel(wheel) {}

		void setParams(const KeyEvent &skey) {
			key = skey;
		}

		void setParams(const MouseButton &smouse) {
			mouse = smouse;
		}

		void setParams(const MouseMove &smove) {
			move = smove;
		}

		void setParams(const MouseWheel &swheel) {
			wheel = swheel;
		}

		union {
			KeyEvent key;
			MouseButton mouse;
			MouseMove move;
			MouseWheel wheel;
		};
	};

	class AbstractListener {
	public:
		virtual void onDraw(int frameNum) = 0;
		virtual void eventHandle(Event event) = 0;
		virtual void onKeyPressed(KeyEvent key) {};
		virtual void onKeyReleased(KeyEvent key) {};
		virtual void onMouseMove(MouseMove move) {};
		virtual void onMousePressed(MouseButton button) {};
		virtual void onMouseReleased(MouseButton button) {};
		virtual void onMouseScroll(MouseWheel wheel) {};
	};
}
#endif //ENGINE0_EVENTS_H
