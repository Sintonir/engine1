//
// Created by Саша on 30.08.2018.
//

#ifndef ENGINE0_BASE_H
#define ENGINE0_BASE_H
#include <string>
#include <stdint.h>
#include <memory>
#include <vector>
#include <queue>
#include <map>

struct Rect {
	int x = 0;
	int y = 0;
	int width;
	int height;
	Rect() = default;
	Rect(const Rect &rect) {
		x = rect.x;
		y = rect.y;
		width = rect.width;
		height = rect.height;
	}

	Rect(const int &x, const int &y, const int &w, const int &h)
		: x(x), y(y), width(w), height(h) {}

};
struct Timespan {
	int start = 0;
	int finish = 0;
	Timespan() = default;
	Timespan(int start, int finish)
		: start(start), finish(finish) {}
	Timespan(const Timespan & span)
		: start(span.start), finish(span.finish) {}
};


class AbstractImage{
public:
	AbstractImage(const AbstractImage &abstractImage) = default;
	AbstractImage() = default;
	virtual ~AbstractImage() = default;
	virtual unsigned int getWidth() const = 0;
	virtual unsigned int getHeight() const = 0;
	virtual std::unique_ptr<AbstractImage> copy() = 0;
};

class AbstractRender {
public:
	virtual ~AbstractRender() = default;
	virtual std::unique_ptr<AbstractImage> loadImageFull(std::string path) = 0;
	virtual void handleEvents() = 0;
	virtual bool initWindow(int width, int height) = 0;
	virtual bool drawImage(const AbstractImage & image, int x, int y, int width, int height) = 0;
	bool drawImage(const AbstractImage & image, Rect rect) { return drawImage(image, rect.x, rect.y, rect.width, rect.height); }
};
typedef std::unique_ptr<AbstractImage> pAImage;

#endif //ENGINE0_BASE_H
