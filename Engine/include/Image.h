//
// Created by Саша on 23.08.2018.
//

#ifndef UNTITLED_IMAGE_H
#define UNTITLED_IMAGE_H

#include <SFML/Graphics.hpp>
#include <string>
#include <stdint.h>
#include <memory>
#include "AbstractLowLevel.h"
template <typename TextureType>
class TemplateImage :public AbstractImage {
public:
	TextureType texture;
	virtual ~TemplateImage() = default;
	TemplateImage(const TemplateImage &templateImage) = default;
	TemplateImage() = default;

	TemplateImage(TextureType texture) : texture(texture) {}

};

#endif //UNTITLED_IMAGE_H
