//
// Created by Саша on 23.08.2018.
//
#include "AbstractLowLevel.h"
#include <vector>
#ifndef UNTITLED_ANIMATION_H
#define UNTITLED_ANIMATION_H

class Animation {
public:
	Animation();
	Animation(const Animation &that);
	Animation(std::vector<pAImage> &&images);

	void addImage(pAImage &&im);
	AbstractImage & getFrameByNum(int frame);

	unsigned int getLength() const;

private:
	std::vector<pAImage> images;
};


#endif //UNTITLED_ANIMATION_H
