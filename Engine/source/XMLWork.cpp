//
// Created by Саша on 02.09.2018.
//

#include "XMLWork.h"

std::unique_ptr<Engine> XMLWork::loadFromXML(std::string path) {
	pugi::xml_document document;
	pugi::xml_parse_result parse_result = document.load_file(path.c_str());
	pugi::xml_node root = document.child("Engine");

	int lapse = atoi(root.attribute("lapse").value());
	int width = atoi(root.attribute("width").value());
	int height = atoi(root.attribute("height").value());
	std::unique_ptr<Engine> engine(new Engine(lapse,width,height));
	for (pugi::xml_node GObject : root.children("GameObject")) {
		int x = atoi(GObject.attribute("left").value());
		int y = atoi(GObject.attribute("top").value());
		int start = atoi(GObject.attribute("start").value());
		int finish = atoi(GObject.attribute("finish").value());
		int zOrder = atoi(GObject.attribute("zOrder").value());
		int width_ = atoi(GObject.attribute("width").value());
		int height_ = atoi(GObject.attribute("height").value());
		int lapse = atoi(GObject.attribute("lapse").value());
		std::vector<std::string> vec;
		for (pugi::xml_node TPath : GObject.children("File")) {
			std::string path = TPath.attribute("path").value();
			vec.emplace_back(path);
		}
		engine->addObject(vec, x, y, zOrder, width_, height_, start, finish, lapse);
	}
	return std::move(engine);
}


bool XMLWork::exampleToXML()
{
	pugi::xml_document document;
	pugi::xml_node root = document.append_child("Engine");

	root.append_attribute("lapse").set_value(1000);
	root.append_attribute("width").set_value(1000);
	root.append_attribute("height").set_value(800);
	
	//1th object - image
	pugi::xml_node obj1 = root.append_child("GameObject");
	obj1.append_attribute("left").set_value (0);
	obj1.append_attribute("top").set_value (0);
	obj1.append_attribute("width").set_value (200);
	obj1.append_attribute("height").set_value (200);
	obj1.append_attribute("start").set_value (0);
	obj1.append_attribute("finish").set_value (0);
	obj1.append_attribute("zOrder").set_value (0);

	pugi::xml_node file1 = obj1.append_child("File");
	file1.append_attribute("path").set_value("\\img\\30.jpg");

	//2th object - animation
	pugi::xml_node obj2 = root.append_child("GameObject");
	obj2.append_attribute("left").set_value(200);
	obj2.append_attribute("top").set_value(200);
	obj2.append_attribute("width").set_value(200);
	obj2.append_attribute("height").set_value(200);
	obj2.append_attribute("start").set_value(0);
	obj2.append_attribute("finish").set_value(0);
	obj2.append_attribute("zOrder").set_value(10);
	

	std::vector<std::string> vec;
	vec.push_back("\\img\\29.png");
	vec.push_back("\\img\\28.jpg");
	vec.push_back("\\img\\27.jpg");
	vec.push_back("\\img\\26.jpg");
	vec.push_back("\\img\\25.jpg");
	vec.push_back("\\img\\24.jpg");
	vec.push_back("\\img\\23.jpg");
	vec.push_back("\\img\\22.jpg");
	vec.push_back("\\img\\21.jpg");
	vec.push_back("\\img\\20.jpg");
	vec.push_back("\\img\\19.jpg");
	for (int i = 0; i < vec.size(); i++)
	{
		pugi::xml_node file = obj2.append_child("File");
		file.append_attribute("path").set_value(vec[i].data());
	}
		document.save_file("Example.xml");
	return true;
}
