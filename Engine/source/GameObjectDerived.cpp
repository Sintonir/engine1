#include "GameObject.h"


AnimObject::AnimObject(int lapse, int x, int y, int z, int w, int h, int start, int finish) :
	GameObject(lapse, x, y, z, w, h, start, finish), anim()
{
}

void AnimObject::addImage(pAImage &&image) {
	anim.addImage(std::move(image));
}

bool AnimObject::isFrameEmpty() const
{
	return anim.getLength() == 0;
}

const AbstractImage &AnimObject::getFrameByNumber(int i) {
	return   anim.getFrameByNum(i / lapse_);
}

ImageObject::ImageObject(pAImage && image, int x, int y, int z, int w, int h, int start, int finish)
	: GameObject(0, x, y, z, w, h, start, finish), image(std::move(image))
{
}

const AbstractImage &ImageObject::getFrameByNumber(int i) {
	return *image;
}

bool ImageObject::isFrameEmpty() const
{
	return image->getHeight() == 0 || image->getWidth() == 0;
}




