//
// Created by Саша on 23.08.2018.
//
#include "GameObject.h"

int GameObject::OBJ_ID_ = 0;

GameObject::GameObject(int lapse, int x, int y, int zorder, int width, int height, int start, int finish) {
	ID_ = getNextID();
	lapse_ = lapse;
	setRenderRect(x, y, width, height);
	zOrder_ = zorder;
	setVisibility(start, finish);
}

int GameObject::getX() const {
	return renderRect_.x;
}

void GameObject::setX(int x) {
	GameObject::renderRect_.x = x;
}

void GameObject::setRenderRect(int x, int y, unsigned int width, unsigned int height) {
	renderRect_ = Rect(x, y, width, height);
}

int GameObject::getY() const {
	return renderRect_.y;
}

void GameObject::setY(int y) {
	GameObject::renderRect_.y = y;
}

int GameObject::getZOrder() const {
	return zOrder_;
}

void GameObject::setZOrder(int z) {
	zOrder_ = z;
}

int GameObject::getID() const {
	return ID_;
}


int GameObject::getNextID() {
	return OBJ_ID_++;
}

bool GameObject::isVisible(int msec) const
{
	if (isFrameEmpty())
		return false;
	if (visibleInterval_.finish == 0)
		return true;
	return visibleInterval_.start <= msec && visibleInterval_.finish >= msec;
}

void GameObject::setVisibility(int start, int finish) {
	visibleInterval_.start = start;
	visibleInterval_.finish = finish;
}

unsigned int GameObject::getWidth()
{
	return renderRect_.width;
}

unsigned int GameObject::getHeight() {
	return renderRect_.height;
}
void GameObject::setSize(unsigned int w, unsigned int h) {
	renderRect_.height = h;
	renderRect_.width = w;
}
