//
// Created by Саша on 23.08.2018.
//

#include "Scene.h"
#include <thread>
#include <algorithm>
#include <chrono>



Scene::Scene() {
	zSortedKeys_ = std::vector<int>();
}

bool Scene::draw(AbstractRender &render, int FrameNum) {
	for (int i = 0; i < zSortedKeys_.size(); i++) {
		int id = zSortedKeys_[i];
		Rect rect(gameObjs_[id]->getX(), gameObjs_[id]->getY(), gameObjs_[id]->getWidth(), gameObjs_[id]->getHeight());
		if (!(render.drawImage(gameObjs_[id]->getFrameByNumber(FrameNum), rect)))
			return false;
	}
	return true;
}

int Scene::addObj(pGameObj &&obj) {
	int ID = obj->getID();

	gameObjs_.insert(std::pair<int, pGameObj>(obj->getID(), std::move(obj)));
	std::vector<int>::iterator x = GetIteratorByZ(gameObjs_[ID]->getZOrder());
	zSortedKeys_.insert(x, ID);
	return ID;
}
std::vector<int>::iterator Scene::GetIteratorByZ(int z) {
	int res = 0;
	if (gameObjs_.size() <= 1)
		return zSortedKeys_.begin();
	for (std::vector<int>::iterator it = zSortedKeys_.begin(); it != zSortedKeys_.end(); it++) {
		int id = *it;
		if (gameObjs_[id]->getZOrder() >= z) {
			return it;
		}
	}
	return zSortedKeys_.end();
}
bool Scene::removeObj(int ID) {
	auto iter = std::find(zSortedKeys_.begin(), zSortedKeys_.end(), ID);
	if (iter != zSortedKeys_.end())
		zSortedKeys_.erase(iter);
	return gameObjs_.erase(ID);
}

int Scene::getControlID() {
	return controlID_;
}


int Scene::getLength() {
	return gameObjs_.size();
}

void Scene::moveControlledUp() {
	int y = gameObjs_[controlID_]->getY();
	y -= defaultPixelMove;
	if (y < 0)
		y = 0;
	gameObjs_[controlID_]->setY(y);
}

void Scene::moveControlledDown() {
	int y = gameObjs_[controlID_]->getY();
	y += defaultPixelMove;
	int last = winHeight_ - gameObjs_[controlID_]->getHeight();
	if (y > last)
		y = last;
	gameObjs_[controlID_]->setY(y);
}

void Scene::moveControlledLeft() {
	int x = gameObjs_[controlID_]->getX();
	x -= defaultPixelMove;
	if (x < 0)
		x = 0;
	gameObjs_[controlID_]->setX(x);

}

void Scene::moveControlledRight() {
	int x = gameObjs_[controlID_]->getX();
	x += defaultPixelMove;
	int last = winWidth_ - gameObjs_[controlID_]->getWidth();
	if (x > last)
		x = last;
	gameObjs_[controlID_]->setX(x);
}

void Scene::changeControl(int num) {
	controlID_ = num % gameObjs_.size();
}
void Scene::reZOrderControl(int newZ) {
	auto iter = std::find(zSortedKeys_.begin(), zSortedKeys_.end(), controlID_);
	if (iter != zSortedKeys_.end())
		zSortedKeys_.erase(iter);
	std::vector<int>::iterator x = GetIteratorByZ(gameObjs_[controlID_]->getZOrder());
	zSortedKeys_.insert(x, controlID_);
}

