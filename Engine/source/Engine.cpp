//
// Created by Саша on 23.08.2018.
//

#include "Engine.h"

Engine::~Engine() {
}

Engine::Engine(unsigned int lapse, unsigned int winWidth, unsigned int winHeight)
	: render(new SFRender(this)),
	scene(new Scene()),
	lapse_(lapse == 0 ? defaultLapse : lapse),
	winWidth_(winWidth == 0 ? defaultWinWidth : winWidth),
	winHeight_(winHeight == 0 ? defaultWinHeight : winHeight)
{
}


pAImage Engine::loadImage(const std::string &path) {
	return render->loadImageFull(path);
}

void Engine::onDraw(int msec) {
	scene->draw(*render, msec);
	if (msec - currFrame > defaultVMove) {
		currFrame = msec;
		if (tab_pressed_ > 0)
			scene->changeControl(tab_pressed_);
		if (up_pressed_)
			scene->moveControlledUp();
		if (down_pressed_)
			scene->moveControlledDown();
		if (left_pressed_)
			scene->moveControlledLeft();
		if (right_pressed_)
			scene->moveControlledRight();
	}
}

void Engine::eventHandle(Events::Event event) {
	switch (event.type) {
	case Events::EventType::keyPressed:
		onKeyPressed(event.key);
		break;
	case Events::EventType::keyReleased:
		onKeyReleased(event.key);
		break;
	}
}



int Engine::addObject(const std::string & path, int x, int y, int zorder, unsigned int width, unsigned int height, int start, int finish, int lapse) {
	return scene->addObj(std::unique_ptr<GameObject>(new ImageObject(std::move(loadImage(path)), x, y, zorder, width, height, start, finish)));
}

int Engine::addObject(const std::vector<std::string>& paths, int x, int y, int zorder, unsigned int width, unsigned int height, int start, int finish, int lapse)
{
	lapse = lapse == 0 ? lapse_ : lapse;
	if (paths.size() == 1)
		return addObject(paths[0], x, y, zorder, width, height, start, finish, lapse);
	std::unique_ptr<AnimObject> obj = std::make_unique<AnimObject>(lapse, x, y, zorder, width, height, start, finish);

	for (int i = 0; i < paths.size(); i++) {
		obj->addImage(loadImage(paths[i]));
		if (obj->getWidth() == 0 || obj->getHeight() == 0)
			obj->setSize(obj->getFrameByNumber(0).getWidth(), obj->getFrameByNumber(0).getHeight());
	}
	return scene->addObj(std::move(obj));
}

void Engine::startApp() {
	render->initWindow(winWidth_, winHeight_);
}

void Engine::onKeyPressed(Events::KeyEvent key) {
	AbstractListener::onKeyPressed(key);
	switch (key.keyCode)
	{
	case Events::Up:
		up_pressed_ = true;
		break;
	case Events::Down:
		down_pressed_ = true;
		break;
	case Events::Left:
		left_pressed_ = true;
		break;
	case Events::Right:
		right_pressed_ = true;
		break;
	case Events::Tab:
		tab_pressed_++;
		break;

	}
}

void Engine::onKeyReleased(Events::KeyEvent key) {
	AbstractListener::onKeyReleased(key);
	switch (key.keyCode)
	{
	case Events::Up:
		up_pressed_ = false;
		break;
	case Events::Down:
		down_pressed_ = false;
		break;
	case Events::Left:
		left_pressed_ = false;
		break;
	case Events::Right:
		right_pressed_ = false;
		break;
	}
}
