//
// Created by Саша on 23.08.2018.
//

#include <stdarg.h>
#include "Animation.h"


Animation::Animation() {
	images = std::vector<pAImage>();
}

void Animation::addImage(pAImage &&im) {
	images.push_back(std::move(im));
}

AbstractImage &Animation::getFrameByNum(int frame) {
	return *images[frame%getLength()];
}

Animation::Animation(const Animation &that) {
	for (int i = 0; i < that.images.size(); i++)
		images.push_back(that.images[i]->copy());
}

unsigned int Animation::getLength() const {
	return images.size();
}

Animation::Animation(std::vector<pAImage> &&images) {
	for (int i = 0; i < images.size(); i++)
		Animation::images.push_back(std::move(images[i]));
}


