//
// Created by Саша on 23.08.2018.
//
#ifndef UNTITLED_RENDER_H
#define UNTITLED_RENDER_H

#include "Image.h"
#include "Events.h"
#include <queue>
#include <map>
#include <SFML/OpenGL.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

class SfImage :public TemplateImage<sf::Texture> {
public:
	unsigned int getWidth() const override;
	unsigned int getHeight() const override;

	SfImage();

	pAImage copy() override;

	SfImage(const TemplateImage<sf::Texture> &templateImage);

	SfImage(const sf::Texture &texture);
};

class SFRender :public AbstractRender {
public:

	SFRender(Events::AbstractListener *Listener);

	virtual ~SFRender();
	std::unique_ptr<AbstractImage> loadImageFull(std::string path) override;
	bool drawImage(const AbstractImage & image, int x, int y, int width, int height) override;
	bool initWindow(int width, int height);
	void handleEvents();

private:
	sf::RenderWindow window;
	Events::AbstractListener * Listener;
};


#endif //UNTITLED_RENDER_H

