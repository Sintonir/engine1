//
// Created by Саша on 23.08.2018.
//
#include "SFRender.h"

unsigned int SfImage::getWidth() const {
	return texture.getSize().x;
}

unsigned int SfImage::getHeight() const {
	return texture.getSize().y;
}

pAImage SfImage::copy() {
	return std::unique_ptr<SfImage>(new SfImage(texture));
}

SfImage::SfImage() {}

SfImage::SfImage(const sf::Texture &texture) : TemplateImage(texture) {}

SfImage::SfImage(const TemplateImage<sf::Texture> &templateImage) : TemplateImage(templateImage) {}

SFRender::SFRender(Events::AbstractListener *Listener) :/*SpritesToDraw(),*/ Listener(Listener) {
}

SFRender::~SFRender() {
}
std::unique_ptr<AbstractImage> SFRender::loadImageFull(std::string path) {
	std::unique_ptr < SfImage> image = std::make_unique<SfImage>();
	image->texture.loadFromFile(path);
	//	образец приведения типов
	//	std::unique_ptr<AbstractImage> res = std::unique_ptr< AbstractImage>(static_cast<AbstractImage*>(image.release()));
	return image;
}
void SFRender::handleEvents() {

}
bool SFRender::initWindow(int width, int height) {
	window.create(sf::VideoMode(width, height), "", sf::Style::Close);
	window.setVerticalSyncEnabled(true);
	sf::Clock clock;
	// activate the window
	window.setActive(true);
	while (window.isOpen()){
		sf::Event event;
		while (window.pollEvent(event)){
			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
				window.close();
			if (event.type == sf::Event::KeyPressed){
				Events::Event e(Events::keyPressed);
				e.setParams(Events::KeyEvent((Events::Key)event.key.code, event.key.alt, event.key.control, event.key.shift));
				Listener->eventHandle(e);
			}
			if (event.type == sf::Event::KeyReleased){
				Events::Event e(Events::keyReleased);
				e.setParams(Events::KeyEvent((Events::Key)event.key.code, event.key.alt, event.key.control, event.key.shift));
				Listener->eventHandle(e);
			}
			if (event.type == sf::Event::MouseButtonPressed){
				Events::Event e(Events::mouseButtonPressed);
				e.setParams(Events::MouseButton((Events::Button)event.mouseButton.button, event.mouseButton.x, event.mouseButton.y));
				Listener->eventHandle(e);
			}
			if (event.type == sf::Event::MouseButtonReleased){
				Events::Event e(Events::mouseButtonReleased);
				e.setParams(Events::MouseButton((Events::Button)event.mouseButton.button, event.mouseButton.x, event.mouseButton.y));
				Listener->eventHandle(e);
			}
			if (event.type == sf::Event::MouseWheelScrolled){
				Events::Event e(Events::mouseWheel);
				e.setParams(Events::MouseWheel((event.mouseWheelScroll.wheel == sf::Mouse::Wheel::HorizontalWheel), event.mouseWheelScroll.delta, event.mouseWheelScroll.x, event.mouseWheelScroll.y));
				Listener->eventHandle(e);
			}
			if (event.type == sf::Event::MouseMoved){
				Events::Event e(Events::mouseMove);
				e.setParams(Events::MouseMove(event.mouseMove.x, event.mouseMove.y));
				Listener->eventHandle(e);
			}
		}
		// clear the window with white color
		window.clear(sf::Color::White);
		int t = clock.getElapsedTime().asMilliseconds();
		Listener->onDraw(t);
		window.display();
	}
	return true;
}

bool SFRender::drawImage(const AbstractImage & image, int x, int y, int width, int height) {
	sf::Sprite sprite;
	const  SfImage &   img = static_cast <const SfImage&>(image);
	sprite.setTexture(img.texture);

	sf::FloatRect size = sprite.getLocalBounds();
	float xsize = width / size.width;
	float ysize = height / size.height;
	float scale = 1;
	if (xsize != 1 || ysize != 1) {
		if (xsize > ysize)
			scale = ysize;
		else
			scale = xsize;
		if (scale > 0)
			sprite.setScale(scale, scale);
	}
	sprite.setPosition(sf::Vector2f(x, y));
	window.draw(sprite);
	return true;
}



