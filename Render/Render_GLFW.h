//
// Created by Саша on 29.08.2018.
//

#ifndef ENGINE0_RENDER_OPENGL_H
#define ENGINE0_RENDER_OPENGL_H

#include <Gl/glew.h>
#ifdef	_WIN32
#include <GL/wglew.h>
#else
#include <GL/glxew.h>
#endif
//#include <GL/freeglut.h>
#include "Image.h"
#include "GLFW/glfw3.h"

class Render {
public:
    Render() = default;
    void display();
    GLFWwindow* InitWindow(int width, int height);
    bool DrawImage(Image image, Coords coords = Coords(0,0,0));
    Image LoadImage(std::string path);
};


#endif //ENGINE0_RENDER_OPENGL_H
